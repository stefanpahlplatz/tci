package com.matchers;

import com.model.Log;
import org.fest.assertions.Assertions;
import org.fest.assertions.GenericAssert;

public class LogAssert extends GenericAssert<LogAssert, Log> {

    public LogAssert(Log actual){
        super(LogAssert.class,actual);
    }

    public static LogAssert assertThat(Log actual){
        return new LogAssert(actual);
    }

    public LogAssert validLog(int expected_depth, int expected_unique, int expected_pages){
        isNotNull();
        Assertions.assertThat(actual.getId()).isNotNull();
        Assertions.assertThat(actual.getSearch_depth())
                .isEqualTo(expected_depth);
        Assertions.assertThat(actual.getUniques_pages_explored())
                .isEqualTo(expected_unique);
        Assertions.assertThat(actual.getPages_explored())
                .isEqualTo(expected_pages);
        Assertions.assertThat(actual.getTime_elapsed())
                .contains("s");
        return this;
    }
}
