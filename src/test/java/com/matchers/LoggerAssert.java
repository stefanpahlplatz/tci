package com.matchers;

import com.service.Logger;
import org.fest.assertions.*;

public class LoggerAssert extends GenericAssert<LoggerAssert, Logger> {

    public LoggerAssert(Logger actual){
        super(LoggerAssert.class,actual);
    }

    public static LoggerAssert assertThat(Logger actual){
        return new LoggerAssert(actual);
    }

    public LoggerAssert validEndTime(){
        isNotNull();
        String zero_error = "end time must be greater than 0";
        Assertions.assertThat(actual.getEnd_time().getTime())
                .overridingErrorMessage(zero_error)
                .isGreaterThan(0);
        String less_than_start_error = "end time is less than start time";
        Assertions.assertThat(actual.getEnd_time().getTime())
                .overridingErrorMessage(less_than_start_error)
                .isGreaterThanOrEqualTo(actual.getStart_time().getTime());
        return this;
    }
}
