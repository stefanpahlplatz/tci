package com.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MusicTest {
    private Music music = new Music("name", "format", 2019, "artist");

    /**
     * Setting a valid Artist should save the Genre.
     */
    @Test
    public void setArtist() {
        // Act
        music.setArtist("artist2");

        // Assert
        assertEquals(music.getArtist(), "artist2");
    }
}