package com.model;

import com.matchers.LogAssert;
import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * All of the tests for Log.class
 */
public class LogTest {

    private Log log;
    private int expected;


    @Before
    public void init(){

        //Arrange
        this.log = new Log();
        this.expected = 10;
    }

    /**
     *The id must be a Long and cannot be negative
     */
    @Test
    public void whenValidIdEnteredThenSetId(){

        //Act
        log.setId(1l);

        //Assert
        assertThat(log.getId()).isNotNull();
        assertThat(log.getId()).isGreaterThanOrEqualTo(0);
    }

    /**
     * The Id must be set in the constructor of the class to ensure
     * that the Log object must always have an id.
     */
    @Test
    public void whenLogIsInstantiatedThenIdIsNotNull(){

        //Assert
        assertThat(log.getId()).isNotNull();
    }


    /**
     * When the time elapsed is set then afterwards it must be in the format of
     * a String.
     */
    @Test
    public void whenValidTimeElapsedEnteredThenSetTimeElapsed(){

        //Act
        log.setTime_elapsed(10349l);

        //Assert
        assertThat(log.getTime_elapsed()).isNotNull();
    }


    /**
     *Makes sure that when time is elapsed is always positive
     */
    @Test(expected = NumberFormatException.class)
    public void whenTimeElapsedIsNegativeThenThrowException(){

        //Act
        log.setTime_elapsed(-1l);

    }

    /**
     * a valid integer is set for for pages explored and then set
     * in the pages explored variable
     */
    @Test
    public void whenValidPagesExploredValueThenPagesExploredIsSet(){

        //Act
        log.setPages_explored(expected);

        //Assert
        assertThat(log.getPages_explored()).isEqualTo(expected);
    }

    /**
     * Will throw an exception whenever a negative number is set for
     * the pages explored
     */
    @Test(expected = NumberFormatException.class)
    public void whenPagesExploredIsNegativeThenThrowException(){

        //Act
        log.setPages_explored(-1);
    }

    /**
     * makes sure that the unique pages explored is set when the setter is called
     */
    @Test
    public void whenUniquesPagesExploredIsValidThenUniquePagesExploredIsSet(){

        //Act
        log.setPages_explored(expected);
        log.setUniques_pages_explored(expected);

        //Assert
        assertThat(log.getUniques_pages_explored()).isEqualTo(expected);
    }

    /**
     * When the method set unique pages explored is set then if it
     * is less than pages explored throw an exception as this should never happen
     * @throws NumberFormatException
     */
    @Test(expected = NumberFormatException.class)
    public void whenUniquePagesExploredIsLessThanPagesExploredThenThrowException() {

        //Act
        log.setPages_explored(1);
        log.setUniques_pages_explored(2);
    }

    /**
     *Will throw an exception whenever a negative number is set for
     *the unique pages explored variable
     */
    @Test(expected = NumberFormatException.class)
    public void whenUniquePagesExploredIsNegativeThenThrowException(){

        //Act
        log.setUniques_pages_explored(-1);
    }

    /**
     * Will make sure that the search depth variable is set
     * correctly by the setter
     */
    @Test
    public void whenSearchDepthIsValidThenSearchDepthIsSet(){

        //Act
        log.setSearch_depth(expected);

        //Assert
        assertThat(log.getSearch_depth()).isEqualTo(expected);
    }

    /**
     * If the depth search variable is set with a negative integer
     * then an exception will be thrown
     */
    @Test(expected = NumberFormatException.class)
    public void whenSearchDepthIsNegativeThenThrowException(){

        //Act
        log.setSearch_depth(-1);
    }

    @Test
    public void whenInstantiatedWithValuesThenAllFieldsAreSetCorrectly(){

        //Act
        Log log_temp = new Log(23l,2,1,1);

        //Assert
        LogAssert.assertThat(log_temp).validLog(1,1,2);
    }
}
