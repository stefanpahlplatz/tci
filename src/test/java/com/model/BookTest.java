package com.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BookTest {
    private Book book = new Book("name", "format", 2019, "genre", "author", "publisher", "isbn");

    /**
     * Setting a valid Genre should save the Genre.
     */
    @Test
    public void setValidGenre() {
        // Act
        book.setGenre("genre2");

        // Assert
        assertEquals(book.getGenre(), "genre2");
    }

    /**
     * Setting a valid Author should save the Author.
     */
    @Test
    public void setValidAuthor() {
        // Act
        book.setAuthor("author2");

        // Assert
        assertEquals(book.getAuthor(), "author2");
    }

    /**
     * Setting a valid Publisher should save the Publisher.
     */
    @Test
    public void setValidPublisher() {
        // Act
        book.setPublisher("publisher2");

        // Assert
        assertEquals(book.getPublisher(), "publisher2");
    }

    /**
     * Setting a valid Isbn should save the Isbn.
     */
    @Test
    public void setValidIsbn() {
        // Act
        book.setIsbn("isbn2");

        // Assert
        assertEquals(book.getIsbn(), "isbn2");
    }
}