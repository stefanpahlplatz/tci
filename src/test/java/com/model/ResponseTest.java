package com.model;

import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * All of the tests required to fully test the Response.Class
 */
public class ResponseTest {

    private Response response;

    @Before
    public void init(){

        //Arrange
        response = new Response();
    }

    /**
     * Makes sure that the time variable is set when the method setTime
     * is called
     */
    @Test
    public void whenValidTimeIsEnteredSetTime(){

        //Arrange
        int expected = 1000;

        //Act
        response.setTime(expected);

        //Assert
        assertThat(response.getTime()).isEqualTo(expected);
    }

    /**
     * When the set Category method is called that the variable passed
     * is set on the result variable.
     */
    @Test
    public void whenValidCategoryPassedThenSetCategory(){

        //Arrange
        Category category = new Book();

        //Act
        response.setResult(category);

        //Assert
        assertThat(response.getResult()).isEqualTo(category);
    }

    /**
     * If a null value is passed to the Category setter then throw a null pointer
     * exception
     */
    @Test(expected = NullPointerException.class)
    public void whenCategoryEnteredEqualsNullThrowException(){

        //Act
        response.setResult(null);
    }

    /**
     * When the Id variable is set through the setter then
     * the id variable will have a value
     */
    @Test
    public void whenValidIdEnteredThenSetId(){

        //Arrange
        Long expected = 22l;

        //Act
        response.setId(expected);

        //Assert
        assertThat(response.getId()).isEqualTo(expected);
    }

    /**
     * if the id is set with a null value then a null pointer
     * exception is thrown
     */
    @Test(expected = NullPointerException.class)
    public void whenIdIsNullThenThrowException(){

        //Act
        response.setId(null);
    }

    /**
     * whenever a new Response object is created then the id cannot be null
     */
    @Test
    public void whenResponseObjectCreatedIdIsSet(){

        //Assert
        assertThat(response.getId()).isNotNull();
    }
}
