package com.model;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class MovieTest {
    private Movie movie = new Movie("name", "format", 2019, "genre", "director", new ArrayList<>(), new ArrayList<>());

    /**
     * Setting a valid Genre should save the Genre.
     */
    @Test
    public void setValidGenre() {
        // Act
        movie.setGenre("genre2");

        // Assert
        assertEquals(movie.getGenre(), "genre2");
    }

    /**
     * Setting a valid Director should save the Director.
     */
    @Test
    public void setValidDirector() {
        // Act
        movie.setDirector("director2");

        // Assert
        assertEquals(movie.getDirector(), "director2");
    }

    /**
     * Setting a valid Writer should save the Writers.
     */
    @Test
    public void setValidWriters() {
        // Setup
        ArrayList<String> writers = new ArrayList<>();
        writers.add("Writer1");

        // Act
        movie.setWriters(writers);

        // Assert
        assertEquals(movie.getWriters().get(0), "Writer1");
    }

    /**
     * Setting a valid Star should save the Stars.
     */
    @Test
    public void setValidStars() {
        // Setup
        ArrayList<String> stars = new ArrayList<>();
        stars.add("Star1");

        // Act
        movie.setWriters(stars);

        // Assert
        assertEquals(movie.getWriters().get(0), "Star1");
    }
}