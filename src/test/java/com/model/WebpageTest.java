package com.model;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import java.util.HashSet;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class WebpageTest {

    @Rule
    public Timeout timeout = new Timeout(5000);

    /**
     * adds a valid movie to the webpage
     */
    @Test
    public void addValidMovie() {
        //arrange
        Webpage webpage = new Webpage();
        Movie movie = mock(Movie.class);
        //act
        webpage.addMovie(movie);
        //assert
        assertThat(webpage.getMovies().size(),is(1));


    }

    /**
     * adds a null movie to the webpage
     */
    @Test(expected = IllegalArgumentException.class)
    public void addNullMovie() {
        //arrange
        Webpage webpage = new Webpage();
        Movie movie = null;
        //act
        webpage.addMovie(movie);

    }

    /**
     * adds a valid book to the webpage
     */
    @Test
    public void addValidBook() {

        //arrange
        Webpage webpage = new Webpage();
        Book book = mock(Book.class);
        //act
        webpage.addBook(book);
        //assert
        assertThat(webpage.getBooks().size(),is(1));
    }

    /**
     * adds a null book to the webpage
     */
    @Test(expected = IllegalArgumentException.class)
    public void addNullBook() {
        //arrange
        Webpage webpage = new Webpage();
        Book book = null;
        //act
        webpage.addBook(book);
    }

    /**
     * adds a valid music to the webpage
     */
    @Test
    public void addValidMusic() {

        //arrange
        Webpage webpage = new Webpage();
        Music music = mock(Music.class);
        //act
        webpage.addMusic(music);
        //assert
        assertThat(webpage.getMusic().size(),is(1));
    }

    /**
     * adds a null music to the webpage
     */
    @Test(expected = IllegalArgumentException.class)
    public void addNullMusic() {
        //arrange
        Webpage webpage = new Webpage();
        Music music = null;
        //act
        webpage.addMusic(music);
    }

    /**
     * remove a valid movie from the webpage
     */
    @Test
    public void removeValidMovie() {
        //arrange
        Webpage webpage = new Webpage();
        Movie movie = mock(Movie.class);
        //act
        webpage.addMovie(movie);
        webpage.removeMovie(movie);
        //assert
        assertThat(webpage.getMusic().size(),is(0));

    }
    /**
     * if a null movie is passed for removal,throw exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void removeNullMovie() {
        //arrange
        Webpage webpage = new Webpage();
        Movie movie = mock(Movie.class);
        Movie m = null;
        //act
        webpage.addMovie(movie);
        webpage.removeMovie(m);


    }

    /**
     * remove a valid book from the webpage
     */
    @Test
    public void removeValidBook() {

        //arrange
        Webpage webpage = new Webpage();
        Book book = mock(Book.class);
        //act
        webpage.addBook(book);
        webpage.removeBook(book);
        //assert
        assertThat(webpage.getBooks().size(),is(0));
    }

    /**
     * if a null book is passed for removal,throw exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void removeNullBook() {

        //arrange
        Webpage webpage = new Webpage();
        Book book = mock(Book.class);
        Book b = null;
        //act
        webpage.addBook(book);
        webpage.removeBook(b);

    }

    /**
     * remove a valid music from the webpage
     */
    @Test
    public void removeValidMusic() {

        //arrange
        Webpage webpage = new Webpage();
        Music music = mock(Music.class);
        //act
        webpage.addMusic(music);
        webpage.removeMusic(music);
        //assert
        assertThat(webpage.getMusic().size(),is(0));
    }

    /**
     * if a null music is passed for removal,throw exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void removeNullMusic() {

        //arrange
        Webpage webpage = new Webpage();
        Music music = mock(Music.class);
        Music m = null;
        //act
        webpage.addMusic(music);
        webpage.removeMusic(m);

    }

    /**
     * get a hashet with music from webpage
     */
    @Test
    public void getMusics() {

        //arrange
        Webpage webpage = new Webpage();
        Music music = mock(Music.class);
        //act
        webpage.addMusic(music);
        HashSet<Category> musics = webpage.getMusic();
        //assert
        assertThat(musics.size(),is(1));

    }

    /**
     * get a hashet with movies from webpage
     */
    @Test
    public void getMovies() {

        //arrange
        Webpage webpage = new Webpage();
        Movie movie = mock(Movie.class);
        //act
        webpage.addMovie(movie);
        HashSet<Category> movies = webpage.getMovies();
        //assert
        assertThat(movies.size(),is(1));
    }

    /**
     * get a hashet with books from webpage
     */
    @Test
    public void getBooks() {

        //arrange
        Webpage webpage = new Webpage();
        Book book = mock(Book.class);
        //act
        webpage.addBook(book);
        HashSet<Category> books = webpage.getBooks();

        //assert
        assertThat(books.size(),is(1));

    }


    /**
     * if no musics then the hashset should be empty
     */
    @Test
    public void IfNoMusicCountZero() {

        //arrange
        Webpage webpage = new Webpage();
        //act
        HashSet<Category> musics = webpage.getMusic();
        //assert
        assertThat(musics.size(),is(0));


    }

    /**
     * if no books then the hashset should be empty
     */
    @Test
    public void IfNoBooksCountZero() {

        //arrange
        Webpage webpage = new Webpage();

        //act
        HashSet<Category> books = webpage.getBooks();

        //assert
        assertThat(books.size(),is(0));
    }

    /**
     * if no movies then the hashset should be empty
     */
    @Test
    public void IfNoMoviesCountZero() {

        //arrange
        Webpage webpage = new Webpage();
        //act
        HashSet<Category> movies = webpage.getMovies();
        //assert
        assertThat(movies.size(),is(0));
    }

}