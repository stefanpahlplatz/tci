package com.model;

import org.junit.Before;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

public class WebsiteTest {

    private Website website;

    @Before
    public void setUp() {
        this.website = new Website();
    }

    /**
     * Adding a valid webpage should add to the webpages.
     */
    @Test
    public void addValidWebpage() {

        //Arrange
        Webpage webpage = new Webpage();

        //Act
        website.addWebpage(webpage);

        //Assert
        assertThat(website.getWebpages()).contains(webpage);
    }

    /**
     * Adding a null webpage should not increase the webpages.
     */
    @Test
    public void addNullWebpage() {

        //Act
        website.addWebpage(null);

        //Assert
        assertThat(website.getWebpages()).isEmpty();
    }

    /**
     * Removing a valid web page should decrease the webpages.
     */
    @Test
    public void removeValidWebPage() {

        //Arrange
        Webpage webpage = new Webpage();

        //Act
        website.addWebpage(webpage);
        website.removeWebPage(webpage);

        //Assert
        assertThat(website.getWebpages()).isEmpty();
    }

}