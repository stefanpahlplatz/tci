package com.rest;


import com.model.Category;
import com.model.Log;
import com.model.Response;
import com.model.Website;
import com.service.Spider;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static com.Constants.BASE_URL;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.whenNew;

/**
 * All of the tests required to fully test the CrawlerController.Class
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest(CrawlerController.class)
public class CrawlerControllerTest {

    @ClassRule
    public static CrawlerController crawlerController = new CrawlerController();
    @Rule
    public Timeout timeout = new Timeout(5000);

    /**
     * Get all web pages from a web site by url
     */
    @Test
    public void findAllSuccess() throws Exception {
        //arrange
        String validURL = BASE_URL;
        Spider s = mock(Spider.class);
        Website ws = mock(Website.class);
        whenNew(Spider.class).withAnyArguments().thenReturn(s);
        when(s.crawl()).thenReturn(ws);
        CrawlerController cc = spy(crawlerController);
        //act
        Website website = cc.findAll(validURL);
        //assert
        Assert.assertNotNull(website);
    }


    /**
     * If url is null throw exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void findAllUrlNull() {
        //arrange
        String nullURL = null;
        //act
        crawlerController.findAll(nullURL);

    }

    /**
     * If url is an empty string throw exception
     */
    @Test(expected = IllegalArgumentException.class)
    public void findAllUrlEmpty() {

        //arrange
        String validURL = "";
        //act
        crawlerController.findAll(validURL);

    }


    /**
     * Find a category from a url based on type and keyword
     */
    @Test
    public void findCategorySuccess() throws Exception {
        //arrange
        String validURL = BASE_URL;
        String validType = "Book";
        String validKeyword = "The Clean Coder: A Code of Conduct for Professional Programmers";
        Spider s = mock(Spider.class);
        Category cat = mock(Category.class);
        whenNew(Spider.class).withAnyArguments().thenReturn(s);
        when(s.crawl(validType, validKeyword)).thenReturn(cat);
        CrawlerController cc = spy(crawlerController);
        //act
        Response category = crawlerController.findCategory(validURL, validType, validKeyword);
        //assert
        Assert.assertNotNull(category);

    }

    /**
     * Throw exception if the keyword is null
     */
    @Test(expected = IllegalArgumentException.class)
    public void findCategoryNullKeyword() {

        //arrange
        String validURL = BASE_URL;
        String validType = "Book";
        String nullKeyword = null;

        //act
        crawlerController.findCategory(validURL, validType, nullKeyword);

    }

    /**
     * Throw exception if the type is null
     */
    @Test(expected = IllegalArgumentException.class)
    public void findCategoryNullType() {

        //arrange
        String validURL = BASE_URL;
        String nullType = null;
        String validKeyword = "The Clean Coder: A Code of Conduct for Professional Programmers";

        //act
        Response category = crawlerController.findCategory(validURL, nullType, validKeyword);
        //assert
        Assert.assertNotNull(category);
    }

    /**
     * Throw exception if the type is an empty string
     */
    @Test(expected = IllegalArgumentException.class)
    public void findCategoryEmptyType() {
        //arrange
        String validURL = BASE_URL;
        String emptyType = "";
        String validKeyword = "The Clean Coder: A Code of Conduct for Professional Programmers";

        //act
        crawlerController.findCategory(validURL, emptyType, validKeyword);

    }

    /**
     * Throw exception if the keyword is an empty string
     */
    @Test(expected = IllegalArgumentException.class)
    public void findCategoryEmptyKeyword() {
        //arrange
        String validURL = BASE_URL;
        String validType = "Book";
        String emptyKeyword = "";

        //act
        crawlerController.findCategory(validURL, validType, emptyKeyword);

    }

    /**
     * Throw exception if the url is an empty string
     */
    @Test(expected = IllegalArgumentException.class)
    public void findCategoryEmptyURL() {
        //arrange
        String emptyURL = "";
        String validType = "Book";
        String validKeyword = "The Clean Coder: A Code of Conduct for Professional Programmers";

        //act
        Response category = crawlerController.findCategory(emptyURL, validType, validKeyword);
        //assert
        Assert.assertNotNull(category);
    }

    /**
     * Throw exception if the url is null
     */
    @Test(expected = IllegalArgumentException.class)
    public void findCategoryNullURL() {
        //arrange
        String nullURL = null;
        String validType = "Book";
        String validKeyword = "The Clean Coder: A Code of Conduct for Professional Programmers";

        //act
        crawlerController.findCategory(nullURL, validType, validKeyword);

    }

    /**
     * If nothing was crawled then return log null
     */
    @Test
    public void getLastCrawlNull() {
        //act
        Log log = crawlerController.getLastCrawl();
        //assert
        Assert.assertNull(log);
    }

    /**
     * Get last crawl log
     */
    @Test
    public void getLastCrawlSuccess() throws Exception {
        //arrange
        CrawlerController cc = spy(crawlerController);
        String validURL = BASE_URL;
        Spider spider = mock(Spider.class);
        Log l = mock(Log.class);
        Website website =mock(Website.class);
        whenNew(Spider.class).withAnyArguments().thenReturn(spider);
        when(spider.getLog()).thenReturn(l);
        when(spider.crawl()).thenReturn(website);

        //act
        cc.findAll(validURL);
        Log log = cc.getLastCrawl();
        //assert
        Assert.assertNotNull(log);
    }




}
