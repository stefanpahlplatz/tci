package com.service;

import com.model.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Assert;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static com.Constants.BASE_URL;
import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.whenNew;


/**
 * All of the tests required to fully test the Parser.Class
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(Parser.class)
public class ParserTest {

    @ClassRule
    public static String base_url = BASE_URL;
    @Rule
    public Timeout timeout = new Timeout(5000);

    /**
     * Get all links from a web page
     * Direct output
     */
    @Test
    public void getLinksSuccess() {
        //arrange
        String test_url = "details.php?id=201";
        Parser parser = new Parser(base_url + test_url);
        Parser p = spy(parser);
        HashSet<String> expected = new HashSet<>();
        expected.add(base_url + "catalog.php");
        expected.add(base_url + "catalog.php?cat=books");
        expected.add(base_url + "catalog.php?cat=movies");
        expected.add(base_url + "catalog.php?cat=music");
        expected.add(base_url + "suggest.php");
        Document d = Jsoup.parse("<html class=\"gr__localhost\"><head>\n" +
                "\t<title>Forrest Gump</title>\n" +
                "\t<link rel=\"stylesheet\" href=\"css/style.css\" type=\"text/css\">\n" +
                "</head>\n" +
                "<body data-gr-c-s-loaded=\"true\">\n" +
                "\n" +
                "\t<div class=\"header\">\n" +
                "\n" +
                "\t\t<div class=\"wrapper\">\n" +
                "\n" +
                "\t\t\t<h1 class=\"branding-title\"><a href=\"catalog.php\">Personal Media Library</a></h1>\n" +
                "\n" +
                "\t\t\t\t\t<ul class=\"nav\">\n" +
                "                <li class=\"books \"><a href=\"catalog.php?cat=books\">Books</a></li>\n" +
                "\n" +
                "                <li class=\"movies \"><a href=\"catalog.php?cat=movies\">Movies</a></li>\n" +
                "\n" +
                "                <li class=\"music \"><a href=\"catalog.php?cat=music\">Music</a></li>\n" +
                "\n" +
                "                <li class=\"suggest on\"><a href=\"suggest.php\">Suggest</a></li>\n" +
                "            </ul>\n" +
                "\n" +
                "\t\t</div>\n" +
                "\n" +
                "\t</div>\n" +
                "\n" +
                "\t<div id=\"content\">\n" +
                "\n" +
                "<div class=\"section page\">\n" +
                "  <div class=\"wrapper\">\n" +
                "\n" +
                "    <div class=\"breadcrumbs\">\n" +
                "      <a href=\"catalog.php\">Full Catalog</a>\n" +
                "      &gt; <a href=\"catalog.php?cat=Movies\">movies</a>\n" +
                "      &gt; Forrest Gump    </div>\n" +
                "\n" +
                "    <div class=\"media-picture\">\n" +
                "      <span>\n" +
                "        <img src=\"img/media/forest_gump.jpg\" alt=\"Forrest Gump\">\n" +
                "      </span>\n" +
                "    </div>\n" +
                "\n" +
                "    <div class=\"media-details\">\n" +
                "      <h1>Forrest Gump</h1>\n" +
                "      <table>\n" +
                "        <tbody><tr>\n" +
                "          <th>Category</th>\n" +
                "          <td>Movies</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Genre</th>\n" +
                "          <td>Drama</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Format</th>\n" +
                "          <td>DVD</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Year</th>\n" +
                "          <td>1994</td>\n" +
                "        </tr>\n" +
                "                <tr>\n" +
                "          <th>Director</th>\n" +
                "          <td>Robert Zemeckis</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Writers</th>\n" +
                "          <td>Winston Groom, Eric Roth</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Stars</th>\n" +
                "          <td>Tom Hanks, Rebecca Williams, Sally Field, Michael Conner Humphreys</td>\n" +
                "        </tr>\n" +
                "              </tbody></table>\n" +
                "    </div>\n" +
                "\n" +
                "  </div>\n" +
                "</div>\n" +
                "\n" +
                "\n" +
                "\n" +
                "    </div> <!-- end content-->\n" +
                "\n" +
                "    <div class=\"footer\">\n" +
                "\n" +
                "      <div class=\"wrapper\">\n" +
                "\n" +
                "        <ul>\n" +
                "          <li><a href=\"http://twitter.com/treehouse\">Twitter</a></li>\n" +
                "          <li><a href=\"https://www.facebook.com/TeamTreehouse\">Facebook</a></li>\n" +
                "        </ul>\n" +
                "\n" +
                "        <p>©2019 Personal Media Library</p>\n" +
                "\n" +
                "      </div>\n" +
                "\n" +
                "    </div>\n" +
                "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js\"></script>\n" +
                "    <script type=\"text/javascript\" src=\"js/script.js\"></script>\n" +
                "  \n" +
                "\n" +
                "</body></html>");
        when(p.getHTML()).thenReturn(d);

        //act
        HashSet<String> result = p.getLinks();

        //assert
        assertEquals(expected.size(), result.size());
        if (!result.containsAll(expected)) {
            fail("The result doesn't contain all the expected links");
        }


    }

    /**
     * Get the web page
     * Direct Output
     */
    @Test
    public void getWebpageSuccess() {
        //arrange
        String test_url = "details.php?id=201";
        Parser parser = new Parser(base_url + test_url);
        Parser p = spy(parser);
        Document d = Jsoup.parse("<html class=\"gr__localhost\"><head>\n" +
                "\t<title>Forrest Gump</title>\n" +
                "\t<link rel=\"stylesheet\" href=\"css/style.css\" type=\"text/css\">\n" +
                "</head>\n" +
                "<body data-gr-c-s-loaded=\"true\">\n" +
                "\n" +
                "\t<div class=\"header\">\n" +
                "\n" +
                "\t\t<div class=\"wrapper\">\n" +
                "\n" +
                "\t\t\t<h1 class=\"branding-title\"><a href=\"catalog.php\">Personal Media Library</a></h1>\n" +
                "\n" +
                "\t\t\t\t\t<ul class=\"nav\">\n" +
                "                <li class=\"books \"><a href=\"catalog.php?cat=books\">Books</a></li>\n" +
                "\n" +
                "                <li class=\"movies \"><a href=\"catalog.php?cat=movies\">Movies</a></li>\n" +
                "\n" +
                "                <li class=\"music \"><a href=\"catalog.php?cat=music\">Music</a></li>\n" +
                "\n" +
                "                <li class=\"suggest on\"><a href=\"suggest.php\">Suggest</a></li>\n" +
                "            </ul>\n" +
                "\n" +
                "\t\t</div>\n" +
                "\n" +
                "\t</div>\n" +
                "\n" +
                "\t<div id=\"content\">\n" +
                "\n" +
                "<div class=\"section page\">\n" +
                "  <div class=\"wrapper\">\n" +
                "\n" +
                "    <div class=\"breadcrumbs\">\n" +
                "      <a href=\"catalog.php\">Full Catalog</a>\n" +
                "      &gt; <a href=\"catalog.php?cat=Movies\">movies</a>\n" +
                "      &gt; Forrest Gump    </div>\n" +
                "\n" +
                "    <div class=\"media-picture\">\n" +
                "      <span>\n" +
                "        <img src=\"img/media/forest_gump.jpg\" alt=\"Forrest Gump\">\n" +
                "      </span>\n" +
                "    </div>\n" +
                "\n" +
                "    <div class=\"media-details\">\n" +
                "      <h1>Forrest Gump</h1>\n" +
                "      <table>\n" +
                "        <tbody><tr>\n" +
                "          <th>Category</th>\n" +
                "          <td>Movies</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Genre</th>\n" +
                "          <td>Drama</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Format</th>\n" +
                "          <td>DVD</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Year</th>\n" +
                "          <td>1994</td>\n" +
                "        </tr>\n" +
                "                <tr>\n" +
                "          <th>Director</th>\n" +
                "          <td>Robert Zemeckis</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Writers</th>\n" +
                "          <td>Winston Groom, Eric Roth</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Stars</th>\n" +
                "          <td>Tom Hanks, Rebecca Williams, Sally Field, Michael Conner Humphreys</td>\n" +
                "        </tr>\n" +
                "              </tbody></table>\n" +
                "    </div>\n" +
                "\n" +
                "  </div>\n" +
                "</div>\n" +
                "\n" +
                "\n" +
                "\n" +
                "    </div> <!-- end content-->\n" +
                "\n" +
                "    <div class=\"footer\">\n" +
                "\n" +
                "      <div class=\"wrapper\">\n" +
                "\n" +
                "        <ul>\n" +
                "          <li><a href=\"http://twitter.com/treehouse\">Twitter</a></li>\n" +
                "          <li><a href=\"https://www.facebook.com/TeamTreehouse\">Facebook</a></li>\n" +
                "        </ul>\n" +
                "\n" +
                "        <p>©2019 Personal Media Library</p>\n" +
                "\n" +
                "      </div>\n" +
                "\n" +
                "    </div>\n" +
                "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js\"></script>\n" +
                "    <script type=\"text/javascript\" src=\"js/script.js\"></script>\n" +
                "  \n" +
                "\n" +
                "</body></html>");
        when(p.getHTML()).thenReturn(d);
        //act
        Webpage result = p.getWebpage();
        //assert
        Assert.assertNotNull(result);
    }

    /**
     * Get a book by keyword from a web page
     * direct output
     */
    @Test
    public void findBookSuccess() throws Exception {
        //arrange
        String test_url = "details.php?id=102";
        Parser parser = new Parser(base_url + test_url);
        String validKeyword = "The Clean Coder: A Code of Conduct for Professional Programmers";
        HashSet<Category> tbooks = new HashSet<>();
        Book b = new Book("The Clean Coder: A Code of Conduct for Professional Programmers", "Ebook", 2008, "Tech", "Robert C. Martin", "Prentice Hall", "978-0132350884");
        tbooks.add(b);
        Parser p = spy(parser);
        Webpage webpage = mock(Webpage.class);
        whenNew(Webpage.class).withAnyArguments().thenReturn(webpage);
        Document doc = Jsoup.parse("<html class=\"gr__localhost\"><head>\n" +
                "\t<title>Clean Code: A Handbook of Agile Software Craftsmanship</title>\n" +
                "\t<link rel=\"stylesheet\" href=\"css/style.css\" type=\"text/css\">\n" +
                "</head>\n" +
                "<body data-gr-c-s-loaded=\"true\">\n" +
                "\n" +
                "\t<div class=\"header\">\n" +
                "\n" +
                "\t\t<div class=\"wrapper\">\n" +
                "\n" +
                "\t\t\t<h1 class=\"branding-title\"><a href=\"catalog.php\">Personal Media Library</a></h1>\n" +
                "\n" +
                "\t\t\t\t\t<ul class=\"nav\">\n" +
                "                <li class=\"books \"><a href=\"catalog.php?cat=books\">Books</a></li>\n" +
                "\n" +
                "                <li class=\"movies \"><a href=\"catalog.php?cat=movies\">Movies</a></li>\n" +
                "\n" +
                "                <li class=\"music \"><a href=\"catalog.php?cat=music\">Music</a></li>\n" +
                "\n" +
                "                <li class=\"suggest on\"><a href=\"suggest.php\">Suggest</a></li>\n" +
                "            </ul>\n" +
                "\n" +
                "\t\t</div>\n" +
                "\n" +
                "\t</div>\n" +
                "\n" +
                "\t<div id=\"content\">\n" +
                "\n" +
                "<div class=\"section page\">\n" +
                "  <div class=\"wrapper\">\n" +
                "\n" +
                "    <div class=\"breadcrumbs\">\n" +
                "      <a href=\"catalog.php\">Full Catalog</a>\n" +
                "      &gt; <a href=\"catalog.php?cat=Books\">books</a>\n" +
                "      &gt; Clean Code: A Handbook of Agile Software Craftsmanship    </div>\n" +
                "\n" +
                "    <div class=\"media-picture\">\n" +
                "      <span>\n" +
                "        <img src=\"img/media/clean_code.jpg\" alt=\"Clean Code: A Handbook of Agile Software Craftsmanship\">\n" +
                "      </span>\n" +
                "    </div>\n" +
                "\n" +
                "    <div class=\"media-details\">\n" +
                "      <h1>Clean Code: A Handbook of Agile Software Craftsmanship</h1>\n" +
                "      <table>\n" +
                "        <tbody><tr>\n" +
                "          <th>Category</th>\n" +
                "          <td>Books</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Genre</th>\n" +
                "          <td>Tech</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Format</th>\n" +
                "          <td>Ebook</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Year</th>\n" +
                "          <td>2008</td>\n" +
                "        </tr>\n" +
                "                <tr>\n" +
                "          <th>Authors</th>\n" +
                "          <td>Robert C. Martin</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Publisher</th>\n" +
                "          <td>Prentice Hall</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>ISBN</th>\n" +
                "          <td>978-0132350884</td>\n" +
                "        </tr>\n" +
                "              </tbody></table>\n" +
                "    </div>\n" +
                "\n" +
                "  </div>\n" +
                "</div>\n" +
                "\n" +
                "\n" +
                "\n" +
                "    </div> <!-- end content-->\n" +
                "\n" +
                "    <div class=\"footer\">\n" +
                "\n" +
                "      <div class=\"wrapper\">\n" +
                "\n" +
                "        <ul>\n" +
                "          <li><a href=\"http://twitter.com/treehouse\">Twitter</a></li>\n" +
                "          <li><a href=\"https://www.facebook.com/TeamTreehouse\">Facebook</a></li>\n" +
                "        </ul>\n" +
                "\n" +
                "        <p>©2019 Personal Media Library</p>\n" +
                "\n" +
                "      </div>\n" +
                "\n" +
                "    </div>\n" +
                "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js\"></script>\n" +
                "    <script type=\"text/javascript\" src=\"js/script.js\"></script>\n" +
                "  \n" +
                "\n" +
                "</body></html>");
        when(p.getHTML()).thenReturn(doc);
        when(webpage.getBooks()).thenReturn(tbooks);

        //act
        Category book = p.findBook(validKeyword);
        //assert
        assertThat(b).isEqualTo(book);
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();
    /**
     * Throw an exception if the keyword is null
     * direct input
     */
    @Test(expected = IllegalArgumentException.class)
    public void findBookKeywordNull() {
        //arrange
        String test_url = "details.php?id=102";
        Parser parser = new Parser(base_url + test_url);
        String nullKeyword = null;
        expectedEx.expect(IllegalArgumentException.class);
        expectedEx.expectMessage("Employee ID is null");
        //act
        parser.findBook(nullKeyword);
    }

    /**
     * Throw an exception if the keyword is an empty string
     * direct input
     */
    @Test(expected = IllegalArgumentException.class)
    public void findBookKeywordEmpty() {
        //arrange
        String test_url = "details.php?id=102";
        Parser parser = new Parser(base_url + test_url);
        String validKeyword = "";
        //act
        parser.findBook(validKeyword);

    }

    /**
     * Get a  movie by keyword from a web page
     */
    @Test
    public void findMovieSuccess() throws Exception {
        //arrange
        String test_url = "details.php?id=202";
        Parser parser = new Parser(base_url + test_url);
        String validKeyword = "Office Space";
        HashSet<Category> tmovie = new HashSet<>();
        List<String> authors = new ArrayList();
        authors.add("William Goldman");
        List<String> stars = new ArrayList();
        stars.add("Ron Livingston");
        stars.add("Jennifer Aniston");
        stars.add("David Herman");
        stars.add("Ajay Naidu");
        stars.add("Diedrich Bader");
        stars.add("Stephen Root");
        Movie m = new Movie("Office Space", "Blu-ray", 1999, "Comedy", "Mike Judge", authors, stars);
        tmovie.add(m);
        Parser p = spy(parser);
        Webpage webpage = mock(Webpage.class);
        whenNew(Webpage.class).withAnyArguments().thenReturn(webpage);
        Document doc = Jsoup.parse("<html class=\"gr__localhost\"><head>\n" +
                "\t<title>Office Space</title>\n" +
                "\t<link rel=\"stylesheet\" href=\"css/style.css\" type=\"text/css\">\n" +
                "</head>\n" +
                "<body data-gr-c-s-loaded=\"true\">\n" +
                "\n" +
                "\t<div class=\"header\">\n" +
                "\n" +
                "\t\t<div class=\"wrapper\">\n" +
                "\n" +
                "\t\t\t<h1 class=\"branding-title\"><a href=\"catalog.php\">Personal Media Library</a></h1>\n" +
                "\n" +
                "\t\t\t\t\t<ul class=\"nav\">\n" +
                "                <li class=\"books \"><a href=\"catalog.php?cat=books\">Books</a></li>\n" +
                "\n" +
                "                <li class=\"movies \"><a href=\"catalog.php?cat=movies\">Movies</a></li>\n" +
                "\n" +
                "                <li class=\"music \"><a href=\"catalog.php?cat=music\">Music</a></li>\n" +
                "\n" +
                "                <li class=\"suggest on\"><a href=\"suggest.php\">Suggest</a></li>\n" +
                "            </ul>\n" +
                "\n" +
                "\t\t</div>\n" +
                "\n" +
                "\t</div>\n" +
                "\n" +
                "\t<div id=\"content\">\n" +
                "\n" +
                "<div class=\"section page\">\n" +
                "  <div class=\"wrapper\">\n" +
                "\n" +
                "    <div class=\"breadcrumbs\">\n" +
                "      <a href=\"catalog.php\">Full Catalog</a>\n" +
                "      &gt; <a href=\"catalog.php?cat=Movies\">movies</a>\n" +
                "      &gt; Office Space    </div>\n" +
                "\n" +
                "    <div class=\"media-picture\">\n" +
                "      <span>\n" +
                "        <img src=\"img/media/office_space.jpg\" alt=\"Office Space\">\n" +
                "      </span>\n" +
                "    </div>\n" +
                "\n" +
                "    <div class=\"media-details\">\n" +
                "      <h1>Office Space</h1>\n" +
                "      <table>\n" +
                "        <tbody><tr>\n" +
                "          <th>Category</th>\n" +
                "          <td>Movies</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Genre</th>\n" +
                "          <td>Comedy</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Format</th>\n" +
                "          <td>Blu-ray</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Year</th>\n" +
                "          <td>1999</td>\n" +
                "        </tr>\n" +
                "                <tr>\n" +
                "          <th>Director</th>\n" +
                "          <td>Mike Judge</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Writers</th>\n" +
                "          <td>William Goldman</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Stars</th>\n" +
                "          <td>Ron Livingston, Jennifer Aniston, David Herman, Ajay Naidu, Diedrich Bader, Stephen Root</td>\n" +
                "        </tr>\n" +
                "              </tbody></table>\n" +
                "    </div>\n" +
                "\n" +
                "  </div>\n" +
                "</div>\n" +
                "\n" +
                "\n" +
                "\n" +
                "    </div> <!-- end content-->\n" +
                "\n" +
                "    <div class=\"footer\">\n" +
                "\n" +
                "      <div class=\"wrapper\">\n" +
                "\n" +
                "        <ul>\n" +
                "          <li><a href=\"http://twitter.com/treehouse\">Twitter</a></li>\n" +
                "          <li><a href=\"https://www.facebook.com/TeamTreehouse\">Facebook</a></li>\n" +
                "        </ul>\n" +
                "\n" +
                "        <p>©2019 Personal Media Library</p>\n" +
                "\n" +
                "      </div>\n" +
                "\n" +
                "    </div>\n" +
                "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js\"></script>\n" +
                "    <script type=\"text/javascript\" src=\"js/script.js\"></script>\n" +
                "  \n" +
                "\n" +
                "</body></html>");
        when(p.getHTML()).thenReturn(doc);
        when(webpage.getMovies()).thenReturn(tmovie);

        //act
        Category movie = p.findMovie(validKeyword);
        //assert
        assertThat(m).isEqualTo(movie);
    }

    /**
     * Throw an exception if the keyword is null
     */
    @Test(expected = IllegalArgumentException.class)
    public void findMovieKeywordNull() {
        //arrange
        String test_url = "details.php?id=202";
        Parser parser = new Parser(base_url + test_url);
        String validKeyword = null;
        //act
        parser.findMovie(validKeyword);

    }

    /**
     * Throw an exception if the keyword is an empty string
     */
    @Test(expected = IllegalArgumentException.class)
    public void findMovieKeywordEmpty() {
        //arrange
        String test_url = "details.php?id=202";
        Parser parser = new Parser(base_url + test_url);
        String validKeyword = "";
        //act
        parser.findMovie(validKeyword);
    }

    /**
     * Get a music by keyword from a web page
     */
    @Test
    public void findMusicSuccess() throws Exception {

        //arrange
        String test_url = "details.php?id=301";
        Parser parser = new Parser(base_url + test_url);
        String validKeyword = "Beethoven: Complete Symphonies";

        HashSet<Category> tmusic = new HashSet<>();
        List<String> authors = new ArrayList();
        authors.add("William Goldman");
        List<String> stars = new ArrayList();
        stars.add("Ron Livingston");
        stars.add("Jennifer Aniston");
        stars.add("David Herman");
        stars.add("Ajay Naidu");
        stars.add("Diedrich Bader");
        stars.add("Stephen Root");
        Music m = new Music("Beethoven: Complete Symphonies", "CD", 2012, "Ludwig van Beethoven");
        tmusic.add(m);
        Parser p = spy(parser);
        Webpage webpage = mock(Webpage.class);
        whenNew(Webpage.class).withAnyArguments().thenReturn(webpage);

        Document doc = Jsoup.parse("<html class=\"gr__localhost\"><head>\n" +
                "\t<title>Beethoven: Complete Symphonies</title>\n" +
                "\t<link rel=\"stylesheet\" href=\"css/style.css\" type=\"text/css\">\n" +
                "</head>\n" +
                "<body data-gr-c-s-loaded=\"true\">\n" +
                "\n" +
                "\t<div class=\"header\">\n" +
                "\n" +
                "\t\t<div class=\"wrapper\">\n" +
                "\n" +
                "\t\t\t<h1 class=\"branding-title\"><a href=\"catalog.php\">Personal Media Library</a></h1>\n" +
                "\n" +
                "\t\t\t\t\t<ul class=\"nav\">\n" +
                "                <li class=\"books \"><a href=\"catalog.php?cat=books\">Books</a></li>\n" +
                "\n" +
                "                <li class=\"movies \"><a href=\"catalog.php?cat=movies\">Movies</a></li>\n" +
                "\n" +
                "                <li class=\"music \"><a href=\"catalog.php?cat=music\">Music</a></li>\n" +
                "\n" +
                "                <li class=\"suggest on\"><a href=\"suggest.php\">Suggest</a></li>\n" +
                "            </ul>\n" +
                "\n" +
                "\t\t</div>\n" +
                "\n" +
                "\t</div>\n" +
                "\n" +
                "\t<div id=\"content\">\n" +
                "\n" +
                "<div class=\"section page\">\n" +
                "  <div class=\"wrapper\">\n" +
                "\n" +
                "    <div class=\"breadcrumbs\">\n" +
                "      <a href=\"catalog.php\">Full Catalog</a>\n" +
                "      &gt; <a href=\"catalog.php?cat=Music\">music</a>\n" +
                "      &gt; Beethoven: Complete Symphonies    </div>\n" +
                "\n" +
                "    <div class=\"media-picture\">\n" +
                "      <span>\n" +
                "        <img src=\"img/media/beethoven.jpg\" alt=\"Beethoven: Complete Symphonies\">\n" +
                "      </span>\n" +
                "    </div>\n" +
                "\n" +
                "    <div class=\"media-details\">\n" +
                "      <h1>Beethoven: Complete Symphonies</h1>\n" +
                "      <table>\n" +
                "        <tbody><tr>\n" +
                "          <th>Category</th>\n" +
                "          <td>Music</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Genre</th>\n" +
                "          <td>Clasical</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Format</th>\n" +
                "          <td>CD</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "          <th>Year</th>\n" +
                "          <td>2012</td>\n" +
                "        </tr>\n" +
                "                  <tr>\n" +
                "            <th>Artist</th>\n" +
                "            <td>Ludwig van Beethoven</td>\n" +
                "          </tr>\n" +
                "              </tbody></table>\n" +
                "    </div>\n" +
                "\n" +
                "  </div>\n" +
                "</div>\n" +
                "\n" +
                "\n" +
                "\n" +
                "    </div> <!-- end content-->\n" +
                "\n" +
                "    <div class=\"footer\">\n" +
                "\n" +
                "      <div class=\"wrapper\">\n" +
                "\n" +
                "        <ul>\n" +
                "          <li><a href=\"http://twitter.com/treehouse\">Twitter</a></li>\n" +
                "          <li><a href=\"https://www.facebook.com/TeamTreehouse\">Facebook</a></li>\n" +
                "        </ul>\n" +
                "\n" +
                "        <p>©2019 Personal Media Library</p>\n" +
                "\n" +
                "      </div>\n" +
                "\n" +
                "    </div>\n" +
                "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js\"></script>\n" +
                "    <script type=\"text/javascript\" src=\"js/script.js\"></script>\n" +
                "  \n" +
                "\n" +
                "</body></html>");
        when(p.getHTML()).thenReturn(doc);
        when(webpage.getMusic()).thenReturn(tmusic);
        //act
        Category music = p.findMusic(validKeyword);
        //assert
        assertThat(m).isEqualTo(music);
    }

    /**
     * Throw an exception if the keyword is null
     */
    @Test(expected = IllegalArgumentException.class)
    public void findMusicKeywordNull() {
        //arrange
        String test_url = "details.php?id=301";
        Parser parser = new Parser(base_url + test_url);
        String validKeyword = null;
        //act
        parser.findMusic(validKeyword);
    }

    /**
     * Throw an exception if the keyword is an empty string
     */
    @Test(expected = IllegalArgumentException.class)
    public void findMusicKeywordEmpty() {
        //arrange
        String test_url = "details.php?id=301";
        Parser parser = new Parser(base_url + test_url);
        String emptyKeyword = "";
        //act
        parser.findMusic(emptyKeyword);
    }

    /**
     * Checks whatever the right URL was assigned to parser
     */
    @Test
    public void shouldCreateParserWithRightURL() {
        //arrange
        Parser parser = new Parser(base_url);
        Parser expectedParser = new Parser(base_url);
        //assert
        assertThat(parser).isEqualTo(expectedParser);

    }


}
