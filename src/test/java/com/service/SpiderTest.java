package com.service;

import com.model.*;
import org.junit.*;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;


import java.util.HashSet;

import static com.Constants.BASE_URL;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;


/**
 * All of the tests required to fully test the Spider.Class
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(Spider.class)
public class SpiderTest {

    Spider spider;
    @ClassRule
    public static String base_page = BASE_URL;

    @Before
    public void init() {
        spider = new Spider(base_page);
    }

    @Rule
    public Timeout timeout = new Timeout(5000);

    /**
     * Crawl a website
     *
     */
    @Test
    public void crawlWebsiteSuccess() throws Exception {
        //arrange
        String base_url = BASE_URL;
        HashSet<String> expected = new HashSet<>();
        expected.add(base_url+"catalog.php");
        expected.add(base_url+"catalog.php?cat=books");
        expected.add(base_url+"catalog.php?cat=movies");
        expected.add(base_url+"catalog.php?cat=music");
        expected.add(base_url+"suggest.php");
        Parser parser = mock(Parser.class);
        whenNew(Parser.class).withAnyArguments().thenReturn(parser);
        when(parser.getLinks()).thenReturn(expected);
        //act
        Website website = spider.crawl();
        //assert
        Assert.assertNotNull(website);

    }

    /**
     * Crawl a website and get a category by type and keyword
     *
     */


    @Test
    public void crawlCategorySuccess() throws Exception {
        //arrange
        String validType = "Books";
        String validKeyword = "The Clean Coder: A Code of Conduct for Professional Programmers";
        String url = BASE_URL + "/details.php?id=104";
        Book b = new Book("The Clean Coder: A Code of Conduct for Professional Programmers","Ebook",2008,"Tech","Robert C. Martin","Prentice Hall","978-0132350884");
        Parser p = mock(Parser.class);
        Spider s = spy(spider);
        whenNew(Parser.class).withAnyArguments().thenReturn(p);
        HashSet<String>links = new HashSet<>();
        links.add(url);
        when(p.getLinks()).thenReturn(links);
        when(p.findBook(validKeyword)).thenReturn(b);
        //act
       Category book = s.crawl(validType,validKeyword);
        //assert
        Assert.assertNotNull(book);
    }

    /**
     * Throw an exception if type is null
     *
     */
    @Test(expected = IllegalArgumentException.class)
    public void crawlCategoryTypeNull() {
        //arrange
        String nullType = null;
        String validKeyword = "The Clean Coder: A Code of Conduct for Professional Programmers";
        //act
        spider.crawl(nullType,validKeyword);

    }

    /**
     *  Throw an exception if type is an empty string
     *
     */
    @Test(expected = IllegalArgumentException.class)
    public void crawlCategoryTypeEmpty() {
        //arrange
        String emptyType = "";
        String validKeyword = "The Clean Coder: A Code of Conduct for Professional Programmers";
        //act
        spider.crawl(emptyType,validKeyword);

    }

    /**
     * Throw an exception if the keyword is null
     *
     */
    @Test(expected = IllegalArgumentException.class)
    public void crawlCategoryKeywordNull() {
        //arrange
        String validType = "Books";
        String nullKeyword = null;
        //act
        spider.crawl(validType,nullKeyword);
    }

    /**
     *  Throw an exception if the keyword is an empty string
     *
     */
    @Test(expected = IllegalArgumentException.class)
    public void crawlCategoryKeywordEmpty() {
        //arrange
        String validType = "Books";
        String emptyKeyword = "";
        //act
        spider.crawl(validType,emptyKeyword);

    }

    /**
     * Get the log of crawling
     *
     */
    @Test
    public void getLogSuccess() throws Exception {

        //arrange
        HashSet<String> expected = new HashSet<>();
        expected.add(base_page + "catalog.php");
        expected.add(base_page + "catalog.php?cat=books");
        expected.add(base_page + "catalog.php?cat=movies");
        expected.add(base_page + "catalog.php?cat=music");
        expected.add(base_page + "suggest.php");
        Spider s = spy(spider);
        Log l = mock(Log.class);
        Logger logger = mock(Logger.class);
        Parser parser = mock(Parser.class);
        whenNew(Logger.class).withAnyArguments().thenReturn(logger);
        whenNew(Parser.class).withAnyArguments().thenReturn(parser);
        when(logger.getLog()).thenReturn(l);
        when(parser.getLinks()).thenReturn(expected);
        //act
        s.crawl();
        Log log = s.getLog();
        //assert
        Assert.assertNotNull(log);

    }
    /**
     * If nothing was crawled, return null
     *
     */
    @Test
    public void getLogNull() {

        //act
        Log log = spider.getLog();
        //assert
        Assert.assertNull(log);

    }

    /**
     * Checks whatever the right URL was assigned to spider
     */
    @Test
    public void shouldCreateParserWithRightURL() {
        //arrange
        Spider spider = new Spider(base_page);
        Spider expectedSpider = new Spider(base_page);
        //assert
        Assert.assertEquals(spider,expectedSpider);

    }
}
