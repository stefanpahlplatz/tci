package com.service;

import com.model.Log;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import com.matchers.LoggerAssert;
import org.junit.rules.Timeout;

import static org.fest.assertions.Assertions.assertThat;


/**
 * all of the test for the methods in the Logger.class
 */
public class LoggerTest {

    private Logger logger;

    @Rule
    public Timeout timeout = new Timeout(3000);

    @Before
    public void init(){

        //Arrange
        logger = new Logger();
    }

    /**
     * Makes sure that start time stamp is set after the setter is called
     * start time stamp cannot be negative and is not null
     */
    @Test
    public void whenTimeStampStartIsValidThenSetTimeStampStartTime() {

        //Assert
        assertThat(logger.getStart_time()).isNotNull();
    }

    /**
     * if the set timestamp end is called then the end timestamp is set
     * and the end timestamp is not less than 0 and is greater than start timestamp
     */
    @Test
    public void whenTimeStampEndIsValidThenSetTimeStampEndTime() throws InterruptedException {

        //Act
        Thread.sleep(100);
        logger.setEnd_time();

        //Assert
        LoggerAssert.assertThat(logger).validEndTime();
    }

    /**
     * The depth of pages searched can only increase and not decrease
     */
    @Test
    public void whenIncreaseDepthIsCalledThenSearchDepthIsGreaterThanBefore(){

        //Arrange
        int depth = logger.getSearch_depth();

        //Act
        logger.increaseDepth();

        //Assert
        assertThat(logger.getSearch_depth()).isGreaterThan(depth);
    }

    /**
     * The number of uniques pages can only increase and not decrease
     */
    @Test
    public void whenIncreaseUniquePagesIsCalledThenUniquePagesIsGreaterThanBefore(){

        //Arrange
        int pages = logger.getUnique_pages_explored();

        //Act
        logger.increaseUniquePagesExplored();

        //Assert
        assertThat(logger.getUnique_pages_explored()).isGreaterThan(pages);
    }

    /**
     * The pages explored cannot be less than unique pages or a problem
     * occurred with logging
     */
    @Test
    public void whenIncreaseUniquePagesIsCalledThenPagesExploredIsNotLessThanUniquesPages(){

        //Arrange
        logger.increaseUniquePagesExplored();

        //Act
        logger.getLog();

        //Assert
        assertThat(logger.getPages_explored()).isGreaterThanOrEqualTo(logger.getUnique_pages_explored());
    }

    /**
     * The number of pages visited can only increase and not decrease
     */
    @Test
    public void whenIncreasePagesIsCalledThenPagesIsGreaterThanBefore(){

        //Arrange
        int pages = logger.getPages_explored();

        //Act
        logger.increaseUniquePagesExplored();
        logger.increasePagesExplored();

        //Assert
        assertThat(logger.getPages_explored()).isGreaterThan(pages);
    }

    /**
     * the returned Log object is never null
     */
    @Test
    public void whenGetLogIsCalledThenReturnedLogCannotBeNull(){

        //Act
        Log log = logger.getLog();

        //Assert
        assertThat(log).isNotNull();
    }

}
