package com.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.io.Serializable;
import java.util.HashSet;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Webpage  implements Serializable {

    private HashSet<Category> movies;
    private HashSet<Category> books;
    private HashSet<Category> music;

    public Webpage()
    {
        movies=new HashSet<>();
        books=new HashSet<>();
        music=new HashSet<>();
    }

    public void addMovie(Category movie){
        if(movie!=null) {
            movies.add(movie);
        }else
        {
            throw new IllegalArgumentException("movie can't be null");
        }
    }

    public void addBook(Category book){
        if(book != null) {
            books.add(book);
        }else
        {
            throw new IllegalArgumentException("book can't be null");
        }
    }

    public void addMusic(Category music){
        if(music != null) {
            this.music.add(music);
        }else
        {
            throw new IllegalArgumentException("music can't be null");
        }
    }

    public void removeMovie(Category movie){
        if(movie != null) {
            movies.remove(movie);
        }else
        {
            throw new IllegalArgumentException("movie can't be null");
        }
    }

    public void removeBook(Category book){
        if(book != null) {
            books.remove(book);
        }else
        {
            throw new IllegalArgumentException("book can't be null");
        }
    }

    public void removeMusic(Category music){
        if(music != null) {
            this.music.remove(music);
        }else
        {
            throw new IllegalArgumentException("music can't be null");
        }
    }

    public HashSet<Category> getMovies() {
        return movies;
    }

    public HashSet<Category> getBooks() {
        return books;
    }

    public HashSet<Category> getMusic() {
        return music;
    }

    @Override
    public boolean equals(Object obj){
        return false;
    }

    @Override
    public int hashCode(){
        return 0;
    }
}
