package com.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicLong;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Log implements Serializable {

    @JsonIgnore
    private static AtomicLong atomicLong = new AtomicLong(0);
    private Long id;
    private String time_elapsed;
    private int pages_explored;
    private int unique_pages_explored;
    private int search_depth;

    public Log(){
        this.id = atomicLong.incrementAndGet();
        this.time_elapsed = "0s";
        this.pages_explored = 0;
        this.unique_pages_explored = 0;
        this.search_depth = 0;
    }

    public Log(Long time_elapsed,int pages_explored,int unique_pages_explored, int search_depth){
        this.id = atomicLong.incrementAndGet();
        this.setTime_elapsed(time_elapsed);
        this.setPages_explored(pages_explored);
        this.setUniques_pages_explored(unique_pages_explored);
        this.setSearch_depth(search_depth);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id){

        if(id != null && id > 0)
            this.id = id;
    }

    public String getTime_elapsed() {
        return time_elapsed;
    }

    public void setTime_elapsed(Long time_elapsed) {

        if(time_elapsed != null && time_elapsed >= 0){
            float time = time_elapsed;

            time = time/1000;

            this.time_elapsed = time+"s";
        }
        else{
            throw new NumberFormatException();
        }
    }

    public int getPages_explored() {
        return pages_explored;
    }

    public void setPages_explored(int pages_explored) {

        if(pages_explored >= 0){
            this.pages_explored = pages_explored;
        }
        else{
            throw new NumberFormatException();
        }
    }

    public int getUniques_pages_explored() {
        return unique_pages_explored;
    }

    public void setUniques_pages_explored(int unique_pages_explored) {

        if(unique_pages_explored >= 0 && unique_pages_explored <= this.pages_explored)
            this.unique_pages_explored = unique_pages_explored;
        else
            throw new NumberFormatException();
    }

    public int getSearch_depth() {
        return search_depth;
    }

    public void setSearch_depth(int search_depth) {

        if(search_depth >= 0)
            this.search_depth = search_depth;
        else
            throw new NumberFormatException();
    }
}
