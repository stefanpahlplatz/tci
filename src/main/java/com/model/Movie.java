package com.model;

import java.io.Serializable;
import java.util.List;

public class Movie extends Category  implements Serializable {

    private String genre;
    private String director;
    private List<String> writers;
    private List<String> stars;

    public Movie(){}

    public Movie(String name, String format, int year, String genre, String director, List<String> writers, List<String> stars) {
        super(name, format, year);
        this.genre = genre;
        this.director = director;
        this.writers = writers;
        this.stars = stars;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public List<String> getWriters() {
        return writers;
    }

    public void setWriters(List<String> writers) {
        this.writers = writers;
    }

    public List<String> getStars() {
        return stars;
    }

    public void setStars(List<String> stars) {
        this.stars = stars;
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj){return true;}
        if (obj == null || getClass() != obj.getClass()){
            return false;
        }
        Movie movie = (Movie)obj;
        return this.getName().equals(movie.getName());
    }

    @Override
    public int hashCode(){
        return 0;
    }
}
