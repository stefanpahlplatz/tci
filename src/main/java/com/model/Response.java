package com.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Response implements Serializable {
    @JsonIgnore
    Date date = new Date();
    @JsonIgnore
    private static AtomicLong atomicLong = new AtomicLong(0);
    private Long id;
    private long time= date.getTime();
    private Category result;

    public Response(){
        this.id = atomicLong.incrementAndGet();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {

        if(id != null)
            this.id = id;
        else
            throw new NullPointerException();
    }

    public long getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public Category getResult() {
        return result;
    }

    public void setResult(Category result) {

        if(result != null)
            this.result = result;
        else
            throw new NullPointerException();
    }
}
