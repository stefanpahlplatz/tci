package com.model;

import java.io.Serializable;

public class Music extends Category implements Serializable {

    private String artist;

    public Music(){}

    public Music(String name, String format, int year, String artist) {
        super(name, format, year);
        this.artist = artist;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    @Override
    public boolean equals(Object obj){

        if (this == obj){return true;}
        if (obj == null || getClass() != obj.getClass()){
            return false;
        }
        Music music = (Music)obj;
        return this.getName().equals(music.getName());
    }

    @Override
    public int hashCode(){
        return 0;
    }
}
