package com.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicLong;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Website implements Serializable {
    @JsonIgnore
    Date date = new Date();
    @JsonIgnore
    private static AtomicLong atomicLong = new AtomicLong(0);
    private Long id;
    private long time =date.getTime();
    private HashSet<Webpage> webpages;

    public Website(){
        this.id = atomicLong.incrementAndGet();
        this.webpages = new HashSet<>();
    }

    public void addWebpage(Webpage webpage){

        if(webpage != null){
            this.webpages.add(webpage);
        }
    }

    public void removeWebPage(Webpage webpage){

        this.webpages.remove(webpage);
    }

    public HashSet<Webpage> getWebpages(){
        return this.webpages;
    }

    public long getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public Long getId() {
        return id;
    }
}
