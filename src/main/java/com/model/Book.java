package com.model;

import java.io.Serializable;

public class Book extends Category implements Serializable {

    private String genre;
    private String author;
    private String publisher;
    private String isbn;

    public Book(){}

    public Book(String name, String format, int year, String genre, String author, String publisher, String isbn) {
        super(name, format, year);
        this.genre = genre;
        this.author = author;
        this.publisher = publisher;
        this.isbn = isbn;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj){return true;}
        if (obj == null || getClass() != obj.getClass()){
            return false;
        }
        Book book = (Book)obj;
        return this.getName().equals(book.getName());
    }

    @Override
    public int hashCode(){
        return 0;
    }
}
