package com.service;

import com.model.Log;

import javax.management.InvalidAttributeValueException;
import java.sql.Timestamp;

public class Logger {

    private Timestamp start_time;
    private Timestamp end_time;
    private int pages_explored;
    private int unique_pages_explored;
    private int search_depth;

    public Logger(){
        this.setStart_time();
        this.search_depth = 0;
        this.pages_explored = 0;
        this.unique_pages_explored = 0;
    }

    public Log getLog(){

        if(this.end_time == null)
            setEnd_time();

        return new Log(this.getElapsedTime(),this.pages_explored,this.unique_pages_explored,this.search_depth);
    }

    public void increaseDepth(){
        this.search_depth++;
    }

    public void increasePagesExplored(){

        try{
            if(this.unique_pages_explored == 0){
                throw new InvalidAttributeValueException();
            }
            this.pages_explored++;
        }
        catch (InvalidAttributeValueException ex){
            System.out.println("increase pages explored in Logger class was called before unique pages explored");
        }
    }

    public void increaseUniquePagesExplored(){
        this.unique_pages_explored++;
        this.pages_explored++;
    }

    public int getSearch_depth() {
        return search_depth;
    }

    public int getPages_explored() {
        return pages_explored;
    }

    public int getUnique_pages_explored() {
        return unique_pages_explored;
    }

    public Timestamp getStart_time() {
        return start_time;
    }

    public Timestamp getEnd_time() {
        return end_time;
    }

    public Long getElapsedTime(){
        return this.end_time.getTime() - this.start_time.getTime();
    }

    public void setStart_time() {

        this.start_time = new Timestamp(System.currentTimeMillis());
    }

    public void setEnd_time() {

        this.end_time = new Timestamp(System.currentTimeMillis());
    }
}
