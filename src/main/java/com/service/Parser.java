package com.service;

import com.model.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static com.Constants.BASE_URL;

public class Parser {

    private String url;
    private String root = BASE_URL;

    public Parser(String URL) {
        this.url = URL;
    }

    public Document getHTML() {
        try {
            return Jsoup.connect(url).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public HashSet<String> getLinks() {
        HashSet<String> temp = new HashSet<>();
        Document doc = getHTML();

        //  Links in page
        Elements links = doc.select("a[href]");

        for (Element link : links) {
            if (!link.text().equals("Twitter") && !link.text().equals("Facebook")) {
                temp.add(root + link.attr("href").toLowerCase());
            }
        }
        return temp;
    }

    public Webpage getWebpage() {
        Webpage webpage = new Webpage();
        boolean book = false;
        boolean movie = false;
        boolean music = false;

        String name = "";
        String format = "";
        int year = 0;
        String genre = "";
        String director = "";
        List<String> writers = new ArrayList<>();
        List<String> stars = new ArrayList<>();
        String artist = "";
        String author = "";
        String publisher = "";
        String isbn = "";

        Document doc = getHTML();

        if (doc.select("tbody").size() > 0) {
            name = doc.select("h1").get(1).text();
            Element table = doc.select("tbody").get(0);
            Elements rows = table.select("tr");

            for (Element row : rows) {
                Element heading = row.select("th").get(0);
                Element col = row.select("td").get(0);
                if (heading.text().equals("Category") && col.text().equals("Books")) {
                    book = true;
                }
                if (heading.text().equals("Category") && col.text().equals("Movies")) {
                    movie = true;
                }
                if (heading.text().equals("Category") && col.text().equals("Music")) {
                    music = true;
                }
                if (book) {
                    if (heading.text().equals("Genre")) {
                        genre = col.text();
                    }
                    if (heading.text().equals("Format")) {
                        format = col.text();
                    }
                    if (heading.text().equals("Year")) {
                        year = Integer.parseInt(col.text());
                    }
                    if (heading.text().equals("Authors")) {
                        writers = Arrays.asList(col.text().split("\\s*,\\s*"));
                    }
                    if (heading.text().equals("Publisher")) {
                        publisher = col.text();
                    }
                    if (heading.text().equals("ISBN")) {
                        isbn = col.text();
                    }
                }
                if (music) {
                    if (heading.text().equals("Genre")) {
                        genre = col.text();
                    }
                    if (heading.text().equals("Format")) {
                        format = col.text();
                    }
                    if (heading.text().equals("Year")) {
                        year = Integer.parseInt(col.text());
                    }
                    if (heading.text().equals("Artist")) {
                        artist = col.text();
                    }
                }
                if (movie) {
                    if (heading.text().equals("Genre")) {
                        genre = col.text();

                    }
                    if (heading.text().equals("Format")) {
                        format = col.text();

                    }
                    if (heading.text().equals("Year")) {
                        year = Integer.parseInt(col.text());

                    }
                    if (heading.text().equals("Director")) {
                        director = col.text();

                    }
                    if (heading.text().equals("Writers")) {
                        writers = Arrays.asList(col.text().split("\\s*,\\s*"));

                    }
                    if (heading.text().equals("Stars")) {
                        stars = Arrays.asList(col.text().split("\\s*,\\s*"));


                    }
                }

            }
        }
        if (book) {
            Book b = new Book(name, format, year, genre, author, publisher, isbn);
            webpage.addBook(b);
        }
        if (movie) {
            Movie m = new Movie(name, format, year, genre, director, writers, stars);
            webpage.addMovie(m);
        }
        if (music) {
            Music mc = new Music(name, format, year, artist);
            webpage.addMusic(mc);
        }
        return webpage;
    }

    public Category findBook(String keyword) {
        if (keyword == null || keyword.equals("")) {
            throw new IllegalArgumentException("keyword can't be null or empty");
        } else {
            for (Category b : getWebpage().getBooks()) {
                if (b.getName().equals(keyword)) {
                    return b;
                }
            }
        }
        return null;
    }

    public Category findMovie(String keyword) {
        if (keyword == null || keyword.equals("")) {
            throw new IllegalArgumentException("keyword can't be null or empty");
        } else {
            for (Category m : getWebpage().getMovies()) {
                if (m.getName().equals(keyword)) {
                    return m;
                }
            }
        }
        return null;
    }

    public Category findMusic(String keyword) {
        if (keyword == null || keyword.equals("")) {
            throw new IllegalArgumentException("keyword can't be null or empty");
        } else {
            for (Category m : getWebpage().getMusic()) {
                if (m.getName().equals(keyword)) {
                    return m;
                }
            }
        }
        return null;
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj){return true;}
        if (obj == null || getClass() != obj.getClass()){
            return false;
        }
        Parser parser = (Parser) obj;
        return this.url.equals(parser.url);
    }
}
