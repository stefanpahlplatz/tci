package com.service;

import com.model.Category;
import com.model.Log;
import com.model.Website;

import java.util.HashSet;

public class Spider {

    private Website website;
    private String base_page;
    private HashSet<String> links;
    private Logger logger;
    private Parser parser;

    public Spider(String base_page) {
        this.base_page = base_page;
        this.links = new HashSet<>();
        this.website = new Website();

    }

    public Website crawl() {
        logger = new Logger();
        Boolean lvlone=false;
        Boolean lvltwo=false;
        HashSet<String>uniqLinks = new HashSet<>();
        parser = new Parser(base_page);
        logger.setStart_time();
        links = parser.getLinks();
        for (String l : parser.getLinks()) {

            if(!uniqLinks.contains(l))
            {
                logger.increaseUniquePagesExplored();
               uniqLinks.add(l);
            }else
            {
                logger.increasePagesExplored();
            }
            if(!lvlone)
            {
                logger.increaseDepth();
                lvlone = true;
            }
            try {
                parser = new Parser(l);
                for (String link : parser.getLinks()) {
                    if(!uniqLinks.contains(link))
                    {
                        logger.increaseUniquePagesExplored();
                        uniqLinks.add(link);
                    }else
                    {
                        logger.increasePagesExplored();
                    }
                    if(!lvltwo)
                    {
                        logger.increaseDepth();
                        lvltwo = true;
                    }
                    if (!links.contains(link)) {
                        website.addWebpage(parser.getWebpage());
                        links.add(link);
                    }
                }
                website.addWebpage(parser.getWebpage());
            } catch (Exception e) {
                System.out.println("\n" + e);
            }
        }

        logger.setEnd_time();
        return website;
    }

    public Category crawl(String type, String keyword) {
        logger = new Logger();
        HashSet<String>uniqLinks = new HashSet<>();
        logger.setStart_time();
        Boolean lvlone=false;
        Boolean lvltwo=false;
        if (type == null || type.equals("") || keyword == null || keyword.equals("")) {
            throw new IllegalArgumentException("type or keyword can't be null or empty");
        } else {
            parser = new Parser(base_page);

            for (String l : parser.getLinks()) {
                if(!uniqLinks.contains(l))
                {
                    logger.increaseUniquePagesExplored();
                    uniqLinks.add(l);
                }else
                {
                    logger.increasePagesExplored();
                }
                if(!lvlone)
                {
                    logger.increaseDepth();
                    lvlone = true;
                }
                Parser parser = new Parser(l);
                logger.increaseDepth();
                if (type.equals("Books")) {
                    if (parser.findBook(keyword) != null) {
                        logger.setEnd_time();
                        return parser.findBook(keyword);
                    }
                }
                if (type.equals("Movies")) {
                    if (parser.findMovie(keyword) != null) {
                        logger.setEnd_time();
                        return parser.findMovie(keyword);
                    }
                }
                if (type.equals("Music")) {
                    if (parser.findMusic(keyword) != null) {
                        logger.setEnd_time();
                        return parser.findMusic(keyword);
                    }
                }

                for (String link : parser.getLinks()) {
                    if(!uniqLinks.contains(link))
                    {
                        logger.increaseUniquePagesExplored();
                        uniqLinks.add(link);
                    }else
                    {
                        logger.increasePagesExplored();
                    }
                    if(!lvltwo)
                    {
                        logger.increaseDepth();
                        lvltwo = true;
                    }
                    parser = new Parser(link);
                    if (type.equals("Books")) {
                        if (parser.findBook(keyword) != null) {
                            logger.setEnd_time();
                            return parser.findBook(keyword);
                        }
                    }
                    if (type.equals("Movies")) {
                        if (parser.findMovie(keyword) != null) {
                            logger.setEnd_time();
                            return parser.findMovie(keyword);
                        }
                    }
                    if (type.equals("Music")) {
                        if (parser.findMusic(keyword) != null) {
                            logger.setEnd_time();
                            return parser.findMusic(keyword);
                        }
                    }
                }

            }
        }
        logger.setEnd_time();
        return null;
    }

    public Log getLog() {
        if(logger!=null) {
            return logger.getLog();
        }else{
            return null;
        }
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj){return true;}
        if (obj == null || getClass() != obj.getClass()){
            return false;
        }
        Spider spider = (Spider)obj;
        return this.base_page.equals(spider.base_page);
    }
}
