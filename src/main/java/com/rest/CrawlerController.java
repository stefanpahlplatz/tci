package com.rest;

import com.model.Log;
import com.model.Response;
import com.model.Webpage;
import com.model.Website;
import com.service.Spider;

public class CrawlerController {

    private Spider spider;


    public Website findAll(String url) {
        if (url == null || url.equals("")) {
            throw new IllegalArgumentException("url can't be null or empty");
        } else {
            spider = new Spider(url);
            Website website = new Website();
            for (Webpage w : spider.crawl().getWebpages())
            {
                if(w.getMovies().size()>0 || w.getBooks().size()>0 || w.getMusic().size()>0)
                {
                    website.addWebpage(w);
                }
            }
            return website;
        }
    }

    public Response findCategory(String url, String type, String keyword) {
        if (url == null || url.equals("") || type == null || type.equals("") || keyword == null || keyword.equals("")) {
            throw new IllegalArgumentException("the url,type or keyword can't be null or empty");
        } else {
            Response response = new Response();
            spider = new Spider(url);
            response.setResult(spider.crawl(type, keyword));
            return response;
        }
    }

    public Log getLastCrawl() {
        if(spider != null) {
            return spider.getLog();
        }else
        {
            return null;
        }

    }
}
