package com;

import com.model.Log;
import com.model.Response;
import com.model.Website;
import com.rest.CrawlerController;
import io.javalin.Javalin;

public class Application {

    public static void main(String[] args) {
        Javalin app = Javalin.create().start(7000);
        final CrawlerController crawlerController = new CrawlerController();

        app.post("/find-all", ctx -> {
            String url = ctx.formParam("url");
            Website results = crawlerController.findAll(url);
            ctx.json(results);
        });

        app.post("/find-category", ctx -> {
            String url = ctx.formParam("url");
            String category = ctx.formParam("category");
            String keyword = ctx.formParam("keyword");
            Response response = crawlerController.findCategory(url, category, keyword);
            ctx.json(response);
        });

        app.post("/last-crawl", ctx -> {
            Log response = crawlerController.getLastCrawl();
            ctx.json(response);
        });
    }
}
