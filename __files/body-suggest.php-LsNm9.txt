<html>
<head>
	<title>Suggest a Media Item</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>

	<div class="header">

		<div class="wrapper">

			<h1 class="branding-title"><a href="catalog.php">Personal Media Library</a></h1>

					<ul class="nav">
                <li class="books "><a href="catalog.php?cat=books">Books</a></li>

                <li class="movies "><a href="catalog.php?cat=movies">Movies</a></li>

                <li class="music "><a href="catalog.php?cat=music">Music</a></li>

                <li class="suggest "><a href="suggest.php">Suggest</a></li>
            </ul>

		</div>

	</div>

	<div id="content">
<div class="section page">
  <div class="wrapper">
    <h1>Suggest a Media Item</h1>

    
    <p>If you think there is something I&rsquo;m missing, let me know! Complete the form to send me an email.</p>    <form method="post" action="suggest.php">
      <table>
        <tr>
          <th><label for="name">Name*</label></th>
          <td><input type="text" name="name" id="name" placeholder="Name" value="" required/></td>
        </tr>
        <tr>
          <th><label for="email">Email*</label></th>
          <td><input type="text" name="email" id="email" placeholder="Email" value="" required/></td>
        </tr>
        </tr>
        <tr>
          <th><label for="category" required>Category*</label></th>
          <td><select id="category" name="category">
            <option value="">Select One</option>
            <option value="Books" >Book</option>
            <option value="Movies" >Movie</option>
            <option value="Music" >Music</option>
          </select></td>
        </tr>
        <tr>
          <th><label for="title">Title*</label></th>
          <td><input id="title" type="title" name="title" placeholder="Title" value="" required/></td>
        </tr>
        <tr>
          <th><label for="format">Format</label></th>
          <td><select id="format" name="format">
              <option value="">Select One</option>
              <optgroup id="books" label="Books">
                <option value="Audio">Audio</option>
                <option value="Ebook">Ebook</option>
                <option value="Hardcover">Hardcover</option>
                <option value="Paperback">Paperback</option>
              </optgroup>
              <optgroup id="movies" label="Movies">
                <option value="Blu-ray">Blu-ray</option>
                <option value="DVD">DVD</option>
                <option value="Streaming">Streaming</option>
                <option value="VHS">VHS</option>
              </optgroup>
              <optgroup id="music" label="Music">
                <option value="Cassette">Cassette</option>
                <option value="CD">CD</option>
                <option value="MP3">MP3</option>
                <option value="Vinyl">Vinyl</option>
              </optgroup>
          </select></td>
      </tr>
      <tr>
          <th>
              <label for="genre">Genre</label>
          </th>
          <td>
              <select id="genre" name="genre">
                  <option value="">Select One</option>
                  <optgroup id="books" label="Books">
                    <option value="Action">Action</option>
                    <option value="Adventure">Adventure</option>
                    <option value="Comedy">Comedy</option>
                    <option value="Fantasy">Fantasy</option>
                    <option value="Historical">Historical</option>
                    <option value="Historical Fiction">Historical Fiction</option>
                    <option value="Horror">Horror</option>
                    <option value="Magical Realism">Magical Realism</option>
                    <option value="Mystery">Mystery</option>
                    <option value="Paranoid">Paranoid</option>
                    <option value="Philosophical">Philosophical</option>
                    <option value="Political">Political</option>
                    <option value="Romance">Romance</option>
                    <option value="Saga">Saga</option>
                    <option value="Satire">Satire</option>
                    <option value="Sci-Fi">Sci-Fi</option>
                    <option value="Tech">Tech</option>
                    <option value="Thriller">Thriller</option>
                    <option value="Urban">Urban</option>
                  </optgroup>
                  <optgroup id="movies" label="Movies">
                    <option value="Action">Action</option>
                    <option value="Adventure">Adventure</option>
                    <option value="Animation">Animation</option>
                    <option value="Biography">Biography</option>
                    <option value="Comedy">Comedy</option>
                    <option value="Crime">Crime</option>
                    <option value="Documentary">Documentary</option>
                    <option value="Drama">Drama</option>
                    <option value="Family">Family</option>
                    <option value="Fantasy">Fantasy</option>
                    <option value="Film-Noir">Film-Noir</option>
                    <option value="History">History</option>
                    <option value="Horror">Horror</option>
                    <option value="Musical">Musical</option>
                    <option value="Mystery">Mystery</option>
                    <option value="Romance">Romance</option>
                    <option value="Sci-Fi">Sci-Fi</option>
                    <option value="Sport">Sport</option>
                    <option value="Thriller">Thriller</option>
                    <option value="War">War</option>
                    <option value="Western">Western</option>
                  </optgroup>
                  <optgroup id="music" label="Music">
                    <option value="Alternative">Alternative</option>
                    <option value="Blues">Blues</option>
                    <option value="Classical">Classical</option>
                    <option value="Country">Country</option>
                    <option value="Dance">Dance</option>
                    <option value="Easy Listening">Easy Listening</option>
                    <option value="Electronic">Electronic</option>
                    <option value="Folk">Folk</option>
                    <option value="Hip Hop/Rap">Hip Hop/Rap</option>
                    <option value="Inspirational/Gospel">Insirational/Gospel</option>
                    <option value="Jazz">Jazz</option>
                    <option value="Latin">Latin</option>
                    <option value="New Age">New Age</option>
                    <option value="Opera">Opera</option>
                    <option value="Pop">Pop</option>
                    <option value="R&B/Soul">R&amp;B/Soul</option>
                    <option value="Reggae">Reggae</option>
                    <option value="Rock">Rock</option>
                  </optgroup>
              </select>
          </td>
      </tr>
      <tr>
        <th><label for="email">Year</label></th>
        <td><input id="year" type="text" name="year" placeholder="YYYY" value="" /></td>
      </tr>
      <tr style="display:none">
          <th><label for="address">Address</label></th>
          <td><input id="address" type="text" name="address"  placeholder="address" />
          <p>Please leave this field blank!</p></td>
        </tr>
        <tr>
          <th><label for="name">Additional Details: </label></th>
          <td><textarea id="details" name="details"></textarea></td>
        </tr>
      </table>
      <input type="submit" value="Send" />
    </form>
    <p>
      Fields marked with an astrisk (*) are required.
    </p>
      </div>
</div>
    </div> <!-- end content-->

    <div class="footer">

      <div class="wrapper">

        <ul>
          <li><a href="http://twitter.com/treehouse">Twitter</a></li>
          <li><a href="https://www.facebook.com/TeamTreehouse">Facebook</a></li>
        </ul>

        <p>&copy;2019 Personal Media Library</p>

      </div>

    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
  </body>
</html>
