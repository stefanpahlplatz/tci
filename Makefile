.PHONY: build

build:
	./gradlew build

test:
	./gradlew test -i

record:
	java -jar wiremock-standalone-2.23.2.jar --port 8001 --proxy-all="https://i364237.hera.fhict.nl/" --record-mappings --verbose

demo:
	java -jar wiremock-standalone-2.23.2.jar --port 8001 --verbose & ./gradlew test -i
